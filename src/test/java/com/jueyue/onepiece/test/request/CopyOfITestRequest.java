package com.jueyue.onepiece.test.request;

import com.jueyue.onepiece.test.entity.BaiduWeatherEntity;
import com.onepiece.requestproxy.annotation.IRequest;
import com.onepiece.requestproxy.annotation.IRequestMethod;
import com.onepiece.requestproxy.annotation.IRequestParam;
import com.onepiece.requestproxy.entity.enums.RequestTypeEnum;

/**
 * 测试接口
 * 
 * @author JueYue 2014年5月17日 - 上午10:23:32
 */
@IRequest("copytestRequest")
public interface CopyOfITestRequest {

    @IRequestMethod(type = RequestTypeEnum.GET, url = "http://api.map.baidu.com/telematics/v3/weather")
    String testGet(@IRequestParam("location") String location,
                   @IRequestParam("output") String output, @IRequestParam("ak") String ak);

    @IRequestMethod(type = RequestTypeEnum.GET, url = "http://api.map.baidu.com/telematics/v3/weather")
    BaiduWeatherEntity testGetEntity(@IRequestParam("location") String location,
                                     @IRequestParam("output") String output,
                                     @IRequestParam("ak") String ak);

}
