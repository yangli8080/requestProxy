package com.onepiece.requestproxy.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Request 请求参数注解
 * @author JueYue
 * 2014年2月20日--上午10:37:02
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Deprecated
public @interface RequestParams {
    /**
     * 是否参会签名计算
     * @return
     */
    boolean isSign() default true;

    /**
     * 顺序 for rest OR signCal
     * 2014年5月15日
     * @return
     */
    int order() default 0;

    /**
     * 参数名称
     * 2014年5月15日
     * @return
     */
    String value();
}
