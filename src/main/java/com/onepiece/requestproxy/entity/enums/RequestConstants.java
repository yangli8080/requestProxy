package com.onepiece.requestproxy.entity.enums;

/**
 * 请求常量
 * @author JueYue
 * @date 2014年5月27日 下午3:51:40
 */
public interface RequestConstants {
    /**
     * 自定义URL
     */
    public static String URL  = "url";
    /**
     * webserver 特殊请求
     */
    public static String SOAP = "soapRequest";

}
